<?php
defined('_JEXEC') or die;

require_once JPATH_ROOT . '/components/com_jcomments/models/jcomments.php';
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
?>
<section id="category" itemscope itemtype="http://schema.org/Blog">
    <?php if (empty($this->lead_items) && empty($this->link_items) && empty($this->intro_items)) : ?>
        <?php if ($this->params->get('show_no_articles', 1)) : ?>
    <div class="category-item noborder">
        <p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
    </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php $itemsType = array('lead_items', 'link_items', 'intro_items'); ?>
    <?php foreach($itemsType as $type) : ?>

    <?php if (!empty($this->$type)) : ?>
        <?php $i = 0; ?>
        <?php $leadingLength = count($this->$type); ?>
            <?php foreach ($this->$type as &$item) : ?>
            <?php $class = ($i == 0 || $i == $leadingLength) ? ' noborder' : ''; ?>
                <div class="category-item<?php echo $class; ?>" itemscope itemtype="http://schema.org/BlogPosting">
                    <?php
                    $this->item = & $item;
                    echo $this->loadTemplate('item');
                    ?>
                </div>
                <?php $i++; ?>
            <?php endforeach; ?>
    <?php endif; ?>
    <?php endforeach; ?>

    <?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
        <?php echo $this->pagination->getPagesLinks(); ?>
    <?php endif; ?>
</section>