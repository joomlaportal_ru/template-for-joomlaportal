<?php
defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

$profileUrl = '#';
if(is_file(JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php'))
{
    require_once JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php';
    $profileUrl = JpuserprofileSiteHelper::getRoute($this->item->created_by);
}


if ($params->get('access-view'))
{
    $link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
}
else
{
    $menu = JFactory::getApplication()->getMenu();
    $active = $menu->getActive();
    $itemId = $active->id;
    $link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
    $returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
    $link = new JUri($link1);
    $link->setVar('return', base64_encode($returnURL));
}

$images = json_decode($this->item->images);
$img = '';
if (isset($images->image_intro) && !empty($images->image_intro)){
    $imgfloat = (empty($images->float_intro)) ? $params->get('float_intro') : $images->float_intro;
    $img = '<img';
    if ($images->image_intro_caption){
        $img .='class="caption"' . ' title="' . htmlspecialchars($images->image_intro_caption) . '"';
    }
    $img .='src="'.htmlspecialchars($images->image_intro).'" alt="'.htmlspecialchars($images->image_intro_alt).'" itemprop="thumbnailUrl"/>';
 }

$countComments = JCommentsModel::getCommentsCount(array(
    'object_id' => (int)$this->item->id,
    'object_group' => 'com_content',
    'published' => 1
));
if(!$countComments){
    $countComments = 'Комментариев нет';
}
else{
    if($countComments < 2)
        $word = 'Комментарий';
    elseif($countComments < 5)
        $word = 'Комментария';
    else
        $word = 'Комментариев';

    $anchor = $countComments == 0 ? '#addcomments' : '#comments';

    $countComments = '<a href="'.$link.$anchor.'">'.$countComments.' '.$word.'</a>';
}
$date = (!empty($this->item->created) && $this->item->created != '0000-00-00 00:00:00')
    ? $this->item->created
    : $this->item->publish_up;
?>
<h2>
    <a href="<?php echo $link; ?>"><?php echo $this->item->title; ?></a>
</h2>
<ul class="item-info">
    <li><?php echo JHtml::_('date', $date, JText::_('DATE_FORMAT_LC1')); ?></li>
    <li><a href="<?php echo $profileUrl; ?>"><?php echo $this->item->author; ?></a></li>
    <li><a href="<?php echo JUri::base().$this->item->category_route; ?>"><?php echo $this->item->category_title; ?></a></li>
    <li class="item-comment">
			<svg viewBox="0 0 100 100" class="svg svg-tip-black">
				<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-tip-black"></use>
			</svg>
			<?php echo $countComments; ?>
		</li>
</ul>
<?php echo $this->item->event->beforeDisplayContent; ?>
<?php echo $this->item->introtext; ?>
<?php echo $img; ?>
<?php echo $this->item->event->afterDisplayContent; ?>

