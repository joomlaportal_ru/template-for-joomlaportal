<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
$useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
	|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author'));

$date = (!empty($this->item->created) && $this->item->created != '0000-00-00 00:00:00')
    ? $this->item->created
    : $this->item->publish_up;
?>

<section id="item" itemscope itemtype="http://schema.org/Article">
    <meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? JFactory::getConfig()->get('language') : $this->item->language; ?>" />
    <h1> <?php echo $this->escape($this->item->title); ?> </h1>

    <?php if ($params->get('show_create_date')) : ?>
    <div class="item-date">
        <?php echo JHtml::_('date', $date, JText::_('DATE_FORMAT_LC1')); ?>
    </div>
    <?php endif; ?>

    <?php if ($canEdit) :
        $url = 'index.php?option=com_content&task=article.edit&a_id=' . $this->item->id . '&return=' . base64_encode(JUri::getInstance());
        echo JHtml::_('link', JRoute::_($url), JText::_('JGLOBAL_EDIT'), array('class' => 'btn btn-primary edit-button'));
    endif; ?>

    <?php
    if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && $this->item->paginationrelative)
    {
        echo $this->item->pagination;
    }
    ?>

    <?php if (isset($images->image_fulltext) && !empty($images->image_fulltext)) : ?>
        <?php $imgfloat = (empty($images->float_fulltext)) ? $params->get('float_fulltext') : $images->float_fulltext; ?>
        <div class="item-type">
            <img src="<?php echo htmlspecialchars($images->image_fulltext); ?>" alt="<?php echo htmlspecialchars($images->image_fulltext_alt); ?>" itemprop="image"/>
        </div>
    <?php endif; ?>
    <?php echo $this->item->event->beforeDisplayContent; ?>
    <div class="item-content">
        <?php echo $this->item->text; ?>
    </div>

    <?php if (isset($urls) && ((!empty($urls->urls_position) && ($urls->urls_position == '1')) || ($params->get('urls_position') == '1'))) : ?>
    <div class="translate-target">
        <?php echo $this->loadTemplate('links'); ?>
    </div>
    <?php endif; ?>

    <?php echo $this->item->event->afterDisplayContent; ?>
</section>