<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

function modChrome_clear_no_head($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
        <?php echo $module->content; ?>
	<?php endif;
}
function modChrome_news($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
        <section id="news-line">
            <div class="container">
                <div class="grid-wrap">
                    <div class="grid-col">
                        <div class="news-line-first cell">
													<svg viewBox="0 0 100 100" class="svg svg-news">
														<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-news"></use>
													</svg>
                            <strong><?php echo $module->title; ?></strong>
                            <a href="/news">Все новости</a>
                        </div>
                        <?php echo $module->content; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
	<?php endif;
}
function modChrome_blog($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
        <section id="from-blog">
            <div class="container">
                <?php if ($module->showtitle != 0) : ?>
                    <div class="grid-wrap">
                        <div class="grid-col">
                            <h2><?php echo $module->title; ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php echo $module->content; ?>
            </div>
        </section>
	<?php endif;
}

function modChrome_mobile_menu($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
        <?php if ($module->showtitle != 0) : ?>
            <li><span class="menu-news active sub-target"><?php echo $module->title; ?></span>
                <div class="submenu">
                    <ul>
                        <?php echo $module->content; ?>
                    </ul>
                </div>
            </li>
        <?php else : ?>
            <?php echo $module->content; ?>
        <?php endif; ?>
	<?php endif;
}

function modChrome_usermenu($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
        <div class="user-info-block">
        <?php if ($module->showtitle != 0) : ?>
            <span class="user-info-label"><?php echo $module->title; ?></span>
        <?php endif; ?>
            <ul>
                <?php echo $module->content; ?>
            </ul>
        </div>
	<?php endif;
}

function modChrome_slide_up_no_head($module, &$params, &$attribs)
{
    if (!empty ($module->content)) :
        $moduleId = 'slide_up_module_'.$module->id;
        $isOpen = (empty($_COOKIE[$moduleId])) ? true : false;
        $addClass = $isOpen ? '' : ' minimal';
        $slideupStyle = $isOpen ? '' : ' style="display: none;"';
        $slideDownStyle = !$isOpen ? '' : ' style="display: none;"';
        ?>
        <div class="best-joomla<?php echo $addClass; ?>" id="<?php echo $moduleId; ?>">
            <?php echo $module->content; ?>
            <span class="slideup-block"<?php echo $slideupStyle; ?>>
							<svg viewBox="0 0 100 100" class="svg svg-toggle">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-toggle"></use>
							</svg>
							Свернуть
						</span>
            <span class="slidedown-block"<?php echo $slideDownStyle; ?>>
							<svg viewBox="0 0 100 100" class="svg svg-toggle">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-toggle"></use>
							</svg>
							Развернуть
						</span>
        </div>
    <?php endif;
}