<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="message">
    <div class="container">
        <div class="grid-wrap">
            <div class="grid-col">
                <span class="top-close">
									<svg viewBox="0 0 100 100" class="svg svg-cross">
										<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-cross"></use>
									</svg>
									Закрыть
								</span>
                <div class="message-wrapper">
                    <span class="ico alert">
											<svg viewBox="0 0 100 100" class="svg svg-alert">
												<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-alert"></use>
											</svg>
										</span>
                    <?php echo $module->content;?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>