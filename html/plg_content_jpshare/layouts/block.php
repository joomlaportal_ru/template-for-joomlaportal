<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPShare
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */
defined('_JEXEC') or die;

JFactory::getDocument()->addScript('//yandex.st/share/share.js',"text/javascript", false, true);
?>
<div class="share-item">
    Поделиться:
    <div class="yashare-auto-init"
         data-yashareL10n="<?php echo substr(JFactory::getLanguage()->getTag(),0,2) ?>"
         data-yashareType="small"
         data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"
         data-yashareTheme="counter"></div>
</div>