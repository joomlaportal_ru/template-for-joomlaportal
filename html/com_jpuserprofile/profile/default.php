<?php
/** @var $this JpuserprofileViewProfile */
defined( '_JEXEC' ) or die; // No direct access
$info = array();
if(!empty($this->item['country'])){
    $info[] = $this->item['country'];
}
if(!empty($this->item['city'])){
    $info[] = $this->item['city'];
}
if(!empty($this->item['dob'])){
    $dob = JFactory::getDate($this->item['dob']);
    $tz = new DateTimeZone(JFactory::getApplication()->get('offset'));
    $now = JFactory::getDate();
    $now->setTimezone($tz);
    $age = $now->diff($dob)->y;
    $info[] = $age.' '.JText::_('COM_JPUSERPROFILE_AGE');
}
$info = implode(', ', $info);
?>
<section itemtype="http://schema.org/Article" itemscope="" id="item">
    <div class="jpuserprofile-item-content"  itemtype="http://schema.org/Person">
        <div itemprop="image" class="left">
            <img alt="<?php echo $this->user->get('name') ?>"
                 src="http://www.gravatar.com/avatar/<?php echo md5(strtolower($this->user->get('email'))); ?>?s=<?php echo $this->config->get('avatar_size', 300); ?>"
                />
        </div>
        <div class="right" itemtype="http://schema.org/Person">
            <div itemprop="name" class="name">
                <?php echo $this->user->get('name') ?>
            </div>

            <?php if(!empty($info)) : ?>
            <div class="info"><?php echo $info; ?></div>
            <?php endif; ?>

            <?php if(!empty($this->item['aboutme'])) : ?>
            <div class="aboutme"><?php echo $this->item['aboutme']; ?></div>
            <?php endif; ?>
            <ul class="social svg-social">
                <?php if(!empty($this->item['website'])) : ?>
                    <li>
                        <a class="website" target="_blank" rel="nofollow"
                           href="<?php echo $this->item['website'] ?>"
                           itemprop="url" title="<?php echo JText::_('COM_JPUSERPROFILE_WEBSITE'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-link">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-link"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(!empty($this->item['vkontakte'])) : ?>
                    <li>
                        <a class="vkontakte" target="_blank" rel="nofollow"
                           href="http://www.vk.com/<?php echo $this->item['vkontakte'] ?>"
                           itemprop="url" title="<?php echo JText::_('COM_JPUSERPROFILE_VKONTAKTE'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-vk">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-vk"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(!empty($this->item['twitter'])) : ?>
                    <li>
                        <a class="twitter" target="_blank" rel="nofollow"
                           href="https://twitter.com/<?php echo $this->item['twitter'] ?>"
                           itemprop="url" title="<?php echo JText::_('COM_JPUSERPROFILE_TWITTER'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-twitter">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-twitter"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(!empty($this->item['joomlaforum'])) : ?>
                    <li>
                        <a class="joomlaforum" target="_blank" rel="nofollow"
                           href="http://joomlaforum.ru/index.php?action=profile;u=<?php echo $this->item['joomlaforum'] ?>"
                           itemprop="url"title="<?php echo JText::_('COM_JPUSERPROFILE_JOOMLAFORUM'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-joomla">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-joomla"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(!empty($this->item['google'])) : ?>
                    <li>
                        <a class="google" target="_blank" rel="nofollow"
                           href="https://plus.google.com/<?php echo $this->item['google'] ?>"
                           itemprop="url" title="<?php echo JText::_('COM_JPUSERPROFILE_GOOGLE'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-googleplus">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-googleplus"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(!empty($this->item['facebook'])) : ?>
                    <li>
                        <a class="facebook" target="_blank" rel="nofollow"
                           href="http://www.facebook.com/<?php echo $this->item['facebook'] ?>"
                           itemprop="url" title="<?php echo JText::_('COM_JPUSERPROFILE_FACEBOOK'); ?>">
	                        <svg viewBox="0 0 100 100" class="svg svg-social-facebook">
		                        <use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-social-facebook"></use>
	                        </svg>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>

	    <div class="clearfix"></div>

        <?php if($this->user->id == JFactory::getUser()->id) : ?>
        <a class="btn btn-primary profile-btn"
           href="<?php echo JRoute::_('index.php?option=com_users&view=profile&layout=edit'); ?>">
            <?php echo JText::_('COM_JPUSERPROFILE_PROFILE_BUTTON'); ?>
        </a>
        <?php endif; ?>

	    <?php if (count($this->articles)): ?>
		    <div class="articles">
			    <strong class="block"><?php echo JText::_('COM_JPUSERPROFILE_ARTICLES_HEADER'); ?></strong>
			    <ul>
				    <?php foreach ($this->articles as $item): ?>
					    <li>
						    <div class="item-autor-left">
							    <?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC1')); ?>
						    </div>
						    <div class="item-autor-right">
							    <a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
						    </div>
					    </li>
				    <?php endforeach; ?>
			    </ul>
		    </div>
	    <?php endif; ?>
    </div>
</section>