<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php');?>" method="post">
    <div>
			<input type="text" name="searchword" placeholder="<?php echo $label; ?>" />
			<svg viewBox="0 0 100 100" class="svg svg-search">
				<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-search"></use>
			</svg>
    </div>
    <div>
        <input type="submit" value="<?php echo $button_text; ?>" />
    </div>
    <input type="hidden" name="task" value="search" />
    <input type="hidden" name="option" value="com_search" />
    <input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
</form>
