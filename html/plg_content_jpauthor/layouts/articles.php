<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPArticlesByAuthor.Layouts
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */
defined('_JEXEC') or die;

$items = isset($displayData['articles']) ? $displayData['articles'] : array();
if (count($items)):
    ?>
    <div class="item-autor-articles">
        <strong class="block"><?php echo JText::_('PLG_CONTENT_JPAUTHOR_ARTICLES_HEADER'); ?></strong>
        <ul>
            <?php foreach ($items as $item): ?>
                <li>
                    <div class="item-autor-left">
                        <?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC1')); ?>
                    </div>
                    <div class="item-autor-right">
                        <a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php
endif;
?>