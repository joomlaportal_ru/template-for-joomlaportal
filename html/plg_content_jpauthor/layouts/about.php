<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPAuthor.Layouts
 *
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */
defined('_JEXEC') or die;
?>
<?php if ($displayData['jpprofile.google']) : ?>
    <a class="autor-name" itemprop="name" href="http://plus.google.com/<?php echo $displayData['jpprofile.google']; ?>/" rel="author">
        <?php echo $displayData['name']; ?>
    </a>
<?php else : ?>
    <span class="autor-name" itemprop="name">
			<?php echo $displayData['name']; ?>
	</span>
<?php endif; ?>
<?php if ($displayData['jpprofile.country']
    || $displayData['jpprofile.city']
    || $displayData['jpprofile.dob']) : ?>
    <span class="autor-info">
<?php if ($displayData['jpprofile.country']) : ?>
    <?php echo $displayData['jpprofile.country']; ?>
<?php endif; ?>
        <?php if ($displayData['jpprofile.country'] && $displayData['jpprofile.city']) : ?>
            ,
        <?php endif; ?>
        <?php if ($displayData['jpprofile.city']) : ?>
            <?php echo $displayData['jpprofile.city']; ?>
        <?php endif; ?>
        <?php if (($displayData['jpprofile.country'] || $displayData['jpprofile.city']) && $displayData['jpprofile.dob']) : ?>
            ,
        <?php endif; ?>
        <?php if ($displayData['jpprofile.dob']) : ?>
            <?php if ($displayData['jpprofile.age']) : ?>
                <?php echo JText::plural('PLG_CONTENT_JPAUTHOR_AGE', $displayData['jpprofile.age']); ?>
            <?php else : ?>
                <?php echo JHtml::_('date', $displayData['jpprofile.dob'], 'd.m.Y'); ?>
            <?php endif; ?>
        <?php endif; ?>
</span>
<?php endif; ?>
<?php if ($displayData['jpprofile.aboutme']) : ?>
    <p itemprop="description" class="jp-author-desc" class="autor-text">
        <?php echo $displayData['jpprofile.aboutme']; ?>
    </p>
<?php endif; ?>
