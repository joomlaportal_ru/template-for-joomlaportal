<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPAuthor.Layouts
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */
defined('_JEXEC') or die;
?>

<div class="item-autor" itemtype="http://schema.org/Person">
    <div class="item-autor-left" itemprop="image">
        <img src="<?php echo $displayData['avatar']; ?>" alt="<?php echo htmlspecialchars($displayData['name']); ?>"/>
    </div>

    <div class="item-autor-right">
        <?php echo JLayoutHelper::render('about', $displayData); ?>
        <?php if ($displayData['jpprofile.facebook']
            || $displayData['jpprofile.website']
            || $displayData['jpprofile.google']
            || $displayData['jpprofile.twitter']
            || $displayData['jpprofile.vkontakte']
            || $displayData['jpprofile.joomlaforum']) :
            echo JLayoutHelper::render('social', $displayData);
        endif; ?>

    </div>
    <div class="clearfix"></div>
    <?php if (isset($displayData['articles'])): ?>
        <?php echo JLayoutHelper::render('articles', $displayData); ?>
    <?php endif; ?>
</div>