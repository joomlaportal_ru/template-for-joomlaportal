<?php
/**
 * @package     Joomlaportal.Plugins
 * @subpackage  Content.JPAuthor.Layouts
 * @author      Joomlaportal.ru Team <smart@joomlaportal.ru>
 * @copyright   Copyright (C) 2013-2014 Joomlaportal.ru. All rights reserved.
 * @license     GNU General Public License version 3 or later; see license.txt
 */

defined('_JEXEC') or die;
?>
<div class="autor-social">
    <?php if ($displayData['jpprofile.website']) : ?>
        <a itemprop="url" class="i-social i-website hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_VISIT_WEBSITE'); ?>" href="<?php echo $displayData['jpprofile.website']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
    <?php if ($displayData['jpprofile.joomlaforum']) : ?>
        <a class="i-social i-joomlaforum hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_JOOMLAFORUM_PROFILE'); ?>" href="http://joomlaforum.ru/index.php?action=profile;u=<?php echo $displayData['jpprofile.joomlaforum']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
    <?php if ($displayData['jpprofile.facebook']) : ?>
        <a class="i-social i-facebook hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_FACEBOOK_PROFILE'); ?>" href="http://www.facebook.com/<?php echo $displayData['jpprofile.facebook']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
    <?php if ($displayData['jpprofile.google']) : ?>
        <a class="i-social i-google hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_GOOGLE_PROFILE'); ?>" href="https://plus.google.com/<?php echo $displayData['jpprofile.google']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
    <?php if ($displayData['jpprofile.twitter']) : ?>
        <a class="i-social i-twitter hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_TWITTER_PROFILE'); ?>" href="https://twitter.com/<?php echo $displayData['jpprofile.twitter']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
    <?php if ($displayData['jpprofile.vkontakte']) : ?>
        <a class="i-social i-vk hasTooltip" data-original-title="<?php echo JText::_('PLG_CONTENT_JPAUTHOR_VK_PROFILE'); ?>" href="http://www.vk.com/<?php echo $displayData['jpprofile.vkontakte']; ?>" rel="nofollow" target="_blank"></a>
    <?php endif; ?>
</div>