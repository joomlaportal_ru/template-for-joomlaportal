<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();
?>
<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search');?>" method="post">
	<div class="search-input">
		<input type="text" name="searchword" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" id="search-searchword" size="30" maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>" class="inputbox" />
		<button name="Search" onclick="this.form.submit()" class="btn"><?php echo JHtml::tooltipText('COM_SEARCH_SEARCH');?></button>

		<div class="clearfix"></div>
		<input type="hidden" name="task" value="search" />
	</div>

	<div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
		<?php if (!empty($this->searchword)):?>
		<?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span class="badge badge-info">' . $this->total . '</span>');?>
		<?php endif;?>

		<label class="custom-checkbox">
			<input type="checkbox" checked /><span></span>
			Поиск по комментариям
		</label>
	</div>

	<div class="phrases formelem">
		<?php echo $this->lists['searchphrase']; ?>
		<?php echo $this->lists['ordering'];?>
		<?php if ($this->total > 0) : ?>
			<?php echo $this->pagination->getLimitBox(); ?>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>

	<?php if ($this->params->get('search_areas', 1)) : ?>
		<div class="only formelem">
		<div class="search-legend"><?php echo JText::_('COM_SEARCH_SEARCH_ONLY');?></div>
		<?php foreach ($this->searchareas['search'] as $val => $txt) :
			$checked = is_array($this->searchareas['active']) && in_array($val, $this->searchareas['active']) ? 'checked="checked"' : '';
		?>
		<label for="area-<?php echo $val;?>" class="checkbox">
			<input type="checkbox" name="areas[]" value="<?php echo $val;?>" id="area-<?php echo $val;?>" <?php echo $checked;?> >
			<?php echo JText::_($txt); ?>
		</label>
			<div class="clearfix"></div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>

</form>

<?php if ($this->total > 0) : ?>
	<p class="counter"><?php echo $this->pagination->getPagesCounter(); ?></p>
<?php endif; ?>
