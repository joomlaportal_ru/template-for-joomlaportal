<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<ul class="search-results<?php echo $this->pageclass_sfx; ?>">
<?php foreach ($this->results as $result) : ?>
	<li>
		<div class="result-title">
			<span class="search-count"><?php echo $this->pagination->limitstart + $result->count . '. ';?></span>
			<?php if ($result->href) :?>
				<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
					<?php echo $this->escape($result->title);?>
				</a>
			<?php else:?>
				<?php echo $this->escape($result->title);?>
			<?php endif; ?>
		</div>
		<p class="result-text">
			<?php echo $result->text; ?>
		</p>
		<div class="result-info">
			<ul class="item-info">
				<?php if ($this->params->get('show_date')) : ?>
				<li>
					<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
				</li>
				<?php endif; ?>
				<?php if ($result->section) : ?>
				<li class="item-comment">
					<svg viewBox="0 0 100 100" class="svg svg-tip-black">
						<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-tip-black"></use>
					</svg>
					<?php echo $this->escape($result->section); ?>
				</li>
				<?php endif; ?>
			</ul>
		</div>
	</li>
<?php endforeach; ?>
</ul>

<div class="category-pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
