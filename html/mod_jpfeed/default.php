<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_feed
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (isset($feed->channel) && isset($feed->channel->item) && count($feed->channel->item))
{
    ?>
    <div class="category-forum">
        <h2>
            <?php echo $module->title; ?>
        </h2>
            <ul>
                <?php
                $i = 0;
                foreach($feed->channel->item as $item)
                {
                    if($i == $limit) break;
                    $i++;

                    $title = !empty($item->title) ? $item->title : '';
                    $link = !empty($item->link) ? $item->link : '';
                    $author = !empty($item->author) ? $item->author : '';
                    $category = !empty($item->category) ? $item->category : '';
                    $categoryLink = !empty($item->categoryLink) ? $item->categoryLink : '';
                    $pubDate = !empty($item->pubDate) ? $item->pubDate : '';
                    $date = new JDate($pubDate);
                    $date = $date->format('d.m.Y');
                    ?>
                    <li>
                        <a href="<?php echo $link; ?>" target="_blank"><?php  echo $title; ?></a>
                        <div class="list-gray">
                                <a href="<?php echo $categoryLink; ?>" target="_blank"><?php  echo $category; ?></a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
    </div>
<?php
}
