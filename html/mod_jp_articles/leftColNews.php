<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="portal-news">
    <h2><?php echo $module->title; ?></h2>
    <a href="http://joomlaportal.ru/rss" class="rss" target="_blank">
			<svg viewBox="0 0 100 100" class="svg svg-rss">
				<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-rss"></use>
			</svg>
			&nbsp;
		</a>
    <div class="clearfix"></div>
    <ul>
        <?php foreach ($list as $item) : ?>

        <li>
          <div class="news-img">
	          <?php if(!empty($item->images->image_intro)) : ?>
              <img src="<?php echo JUri::root().$item->images->image_intro ?>" alt="" />
	          <?php else: ?>
		          <img src="templates/jpnew/images/noimage.png" alt="" />
	          <?php endif; ?>
          </div>
	        <h3><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h3>
	        <p><?php echo strip_tags($item->introtext); ?></p>
        </li>

        <?php endforeach; ?>
    </ul>
</div>


