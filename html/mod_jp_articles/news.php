<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$profileUrl = '#';
$jpuser = false;
if(is_file(JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php'))
{
    require_once JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php';
    $jpuser = true;
}
?>

<?php foreach ($list as $item) : ?>
    <?php if($jpuser) $profileUrl = JpuserprofileSiteHelper::getRoute($item->created_by); ?>
    <div class="news-line-block cell">
        <?php if(!empty($item->images->image_intro)) : ?>
	        <span class="news-image">
            <img src="<?php echo JUri::root().$item->images->image_intro ?>" alt="<?php echo $item->title ?>" />
	        </span>
        <?php endif; ?>
        <a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
        <p><?php echo strip_tags($item->introtext); ?></p>
    </div>

    <div class="news-line-block cell">
        <span class="date-new"><?php echo $item->created; ?></span>
        <a href="<?php echo $profileUrl ?>" class="autor-new"><?php echo $item->author; ?></a><br />
        <a href="<?php echo JUri::base().$item->category_route; ?>">
            <?php echo $item->category_title; ?>
        </a>
    </div>
<?php endforeach; ?>

