<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$profileUrl = '#';
$jpuser = false;
if(is_file(JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php'))
{
    require_once JPATH_ROOT . '/components/com_jpuserprofile/helpers/jpuserprofile.php';
    $jpuser = true;
}
?>
<div class="grid-wrap items">
    <?php foreach ($list as $item) : ?>
        <?php if($jpuser) $profileUrl = JpuserprofileSiteHelper::getRoute($item->created_by); ?>
        <div class="grid-col col-one-third">
            <?php if(!empty($item->images->image_intro)) : ?>
                <img src="<?php echo JUri::root().$item->images->image_intro ?>" alt="<?php echo $item->title ?>" />
            <?php else: ?>
							<svg viewBox="0 0 180 100" class="svg svg-noimage">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-noimage"></use>
							</svg>
            <?php endif; ?>
            <h3><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h3>

            <ul class="item-info">
                <li><?php echo $item->created; ?></li>
                <li><a href="<?php echo $profileUrl ?>"><?php echo $item->author; ?></a></li>
                <li>
                    <a href="<?php echo JUri::base().$item->category_route; ?>">
                        <?php echo $item->category_title; ?>
                    </a>
                </li>
            </ul>
            <p><?php echo strip_tags($item->introtext); ?></p>
        </div>
    <?php endforeach; ?>
    <div class="clearfix"></div>
</div>
