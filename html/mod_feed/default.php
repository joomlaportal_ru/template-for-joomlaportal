<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_feed
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php
if (!empty($feed) && is_string($feed))
{
	echo $feed;
}
else
{
	$lang = JFactory::getLanguage();
	$myrtl = $params->get('rssrtl');
	$direction = " ";

	if ($lang->isRTL() && $myrtl == 0)
	{
		$direction = " redirect-rtl";
	}

	// Feed description
	elseif ($lang->isRTL() && $myrtl == 1)
	{
		$direction = " redirect-ltr";
	}

	elseif ($lang->isRTL() && $myrtl == 2)
	{
		$direction = " redirect-rtl";
	}

	elseif ($myrtl == 0)
	{
		$direction = " redirect-ltr";
	}
	elseif ($myrtl == 1)
	{
		$direction = " redirect-ltr";
	}
	elseif ($myrtl == 2)
	{
		$direction = " redirect-rtl";
	}

	if ($feed != false)
	{
		// Image handling
		$iUrl	= isset($feed->image)	? $feed->image	: null;
		$iTitle = isset($feed->imagetitle) ? $feed->imagetitle : null;
		?>

	<div class="category-forum">
		<h2>
            <?php echo $module->title; ?>
		</h2>

			<?php
	//Show items
	 if (!empty($feed))
	{ ?>
		<ul>
		<?php for ($i = 0; $i < $params->get('rssitems', 5); $i++)
		{
			if (!$feed->offsetExists($i))
			{
				break;
			}
			?>
			<?php
				$uri = (!empty($feed[$i]->uri) || !is_null($feed[$i]->uri)) ? $feed[$i]->uri : $feed[$i]->guid;
				$uri = substr($uri, 0, 4) != 'http' ? $params->get('rsslink') : $uri;
				$text = !empty($feed[$i]->content) ||  !is_null($feed[$i]->content) ? $feed[$i]->content : $feed[$i]->description;
                $feedCategories = $feed[$i]->categories;
                $feedCategories = array_keys($feedCategories);
                $category = (!empty($feedCategories[0])) ? $feedCategories[0] : '';
                $date = (is_object($feed[$i]->updatedDate)) ? $feed[$i]->updatedDate->format('d.m.Y') : '';

            ?>

				<li>
					<?php if (!empty($uri)) : ?>
						<a href="<?php echo $uri; ?>" target="_blank">
						<?php  echo $feed[$i]->title; ?></a>
					<?php else : ?>
						<span class="feed-link"><?php  echo $feed[$i]->title; ?></span>
					<?php  endif; ?>
                    <div class="list-gray">
                        <?php if (!empty($category)) : ?>
                        <a href="javascript:;"><?php  echo $category; ?></a>
                        <?php  endif; ?>
                    </div>
				</li>
		<?php } ?>
		</ul>
	<?php } ?>
	</div>
	<?php }
}
