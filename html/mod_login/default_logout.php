<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$document = JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
?>
<div id="top-userblock" class="user-block top">
    <div class="userblock-open">
        <span class="user-message"></span>
        <div class="user-photo">
            <img alt="<?php echo $user->get('name') ?>"
                 src="http://www.gravatar.com/avatar/<?php
                 echo md5(strtolower($user->get('email'))); ?>?s=40"
                />
        </div>
        <span class="user-name"><?php echo htmlspecialchars($user->get('name')) ?></span>
    </div>

    <div class="user-info">
        <div class="user-info-header">
            <div class="user-photo">
                <img alt="<?php echo $user->get('name') ?>"
                     src="http://www.gravatar.com/avatar/<?php
                     echo md5(strtolower($user->get('email'))); ?>?s=40"
                    />
            </div>
            <span class="user-name"><?php echo $user->get('name') ?></span>
        </div>

        <?php echo $renderer->render('position-14', array('style' => 'usermenu'), null); ?>

        <div class="user-info-block">
            <form action="<?php echo JRoute::_(JUri::getInstance()->toString(), true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-vertical">
                <div class="logout-button">
                    <button type="submit" name="Submit" class="btn btn-primary">
                        <?php echo JText::_('JLOGOUT') ?>
                    </button>
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.logout" />
                    <input type="hidden" name="return" value="<?php echo $return; ?>" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </form>
        </div>
    </div>
</div>

