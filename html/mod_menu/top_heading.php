<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$style = '';
if ($item->menu_image)
{
    $style = ' style="background: url(\''.JUri::root().$item->menu_image.'\') no-repeat scroll 13px 11px rgba(0, 0, 0, 0);"';
}
?><span class="sub-target"<?php echo $style; ?>><?php echo $item->title; ?></span>
