<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$class = !empty($class_sfx) ? $class_sfx : 'col-one-third';
?>
<ul class="grid-col col-one-third">
<?php
foreach ($list as $i => &$item)
{
    $flink = $item->flink;

	if ($flink{0} == '/')
	{
		$flink = trim(JURI::base(), '/') . $flink;
	}
    $flink = JFilterOutput::ampReplace(htmlspecialchars($flink));
    ?>
    <li>
        <a href="<?php echo $flink; ?>"><?php echo $item->title; ?></a>
    </li>
    <?php
}
?>
</ul>
