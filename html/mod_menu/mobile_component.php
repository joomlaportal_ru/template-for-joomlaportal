<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$style = '';
$title = $item->anchor_title ? 'title="' . $item->anchor_title . '" ' : '';
$class = '';

if ($item->id == $active_id)
{
    $class .= ' active';
}

if (in_array($item->id, $path))
{
    $class .= ' active';
}
elseif ($item->type == 'alias')
{
    $aliasToId = $item->params->get('aliasoptions');

    if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
    {
        $class .= ' active';
    }
    elseif (in_array($aliasToId, $path))
    {
        $class .= ' active';
    }
}

if($item->deeper){
    $class .= ' sub-target';
}

if (!empty($class))
{
    $class = ' class="' . trim($class) . '"';
}

if ($item->menu_image)
{
    $style = ' style="background: url(\''.JUri::root().$item->menu_image.'\') no-repeat scroll 13px 11px rgba(0, 0, 0, 0);"';
}

if($item->deeper){
    ?><span <?php echo $class . $style; ?>><?php echo $item->title; ?></span><?php
}
else{
    switch ($item->browserNav)
    {
        default:
        case 0:
            ?><a <?php echo $class . $style; ?>href="<?php echo $item->flink; ?>" <?php echo $title; ?>><?php echo $item->title; ?></a><?php
            break;
        case 1:
            // _blank
            ?><a <?php echo $class . $style; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><?php echo $item->title; ?></a><?php
            break;
        case 2:
            // window.open
            ?><a <?php echo $class . $style; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');return false;" <?php echo $title; ?>><?php echo $item->title; ?></a>
            <?php
            break;
    }
}

