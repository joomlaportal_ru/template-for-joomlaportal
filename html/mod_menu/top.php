<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$menu = & JSite::getMenu();
$mainMenuId = $menu->getDefault()->id;

// Note. It is important to remove spaces between elements.
?>
<ul id="topmenu-list" class="menu">
<?php
foreach ($list as $i => &$item)
{
    if($item->id == $mainMenuId || $item->params->get('aliasoptions') == $mainMenuId)
    {
        continue;
    }
    echo '<li>';

	if ($item->flink{0} == '/') 
	{
		$item->flink = trim(JURI::base(), '/') . $item->flink;
	}

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
		case 'heading':
			require JModuleHelper::getLayoutPath('mod_menu', 'top_' . $item->type);
			break;
		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'top_url');
			break;
	endswitch;

	// The next item is deeper.
	if ($item->deeper)
	{
		echo '<div class="submenu"> <ul class="container">';
	}
	elseif ($item->shallower)
	{
		// The next item is shallower.
		echo '</li>';
		echo str_repeat('</ul> </div></li>', $item->level_diff);
	}
	else
	{
		// The next item is on the same level.
		echo '</li>';
	}
}
?></ul>
