<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

$input = JFactory::getApplication()->input;
$print		= $input->getInt('print');
$Itemid		= $input->getInt('Itemid');
$option		= $input->getCmd('option');
$view		= $input->getCmd('view');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <?php if(!$print): ?>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
    <?php endif; ?>
    <meta name="robots" content="noindex, nofollow" />
</head>
<body class="window contentpane">
    <div id="window">
        <div class="content-window">
            <?php if(!$print): ?>
            <jdoc:include type="message" />
            <?php endif; ?>
    	    <jdoc:include type="component" />
        </div>
    </div>
</body>
</html>