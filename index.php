<?php
$menu = JFactory::getApplication()->getMenu();
$isMainPage = !isset($menu->getActive()->link) || $menu->getActive()->link == "index.php?option=com_content&view=featured";
$document = JFactory::getDocument();
$allowedJoomlaStyles = array(
    '/media/plg_content_jpsyntaxhighlighter/css/shCore.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeDefault.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeDjango.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeEmacs.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeFadeToGrey.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeMidnight.css',
    '/media/plg_content_jpsyntaxhighlighter/css/shThemeRDark.css',
);

$allowedJoomlaScripts = array(
    'http://www.google.com/recaptcha/api/js/recaptcha_ajax.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shCore.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushCss.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushDiff.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushJScript.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushPhp.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushPlain.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushSql.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shBrushXml.js',
    '/media/plg_content_jpsyntaxhighlighter/js/shLegacy.js',
    //'/media/jui/js/bootstrap.min.js',
    //JURI::root(true) . '/components/com_jcomments/js/jcomments-v2.3.js?v=12',
    //JURI::root(true) . '/components/com_jcomments/libraries/joomlatune/ajax.js?v=4',
    '//yandex.st/share/share.js'
);

$disabledScripts = array(
    'jQuery(\'.hasPopover\').popover({"html": true,"trigger": "hover focus","container": "body"});',
                'jQuery(\'.hasTooltip\').tooltip({"html": true,"container": "body"});',
    'new JCaption(\'img.caption\');'
);

$javaScript = str_replace($disabledScripts, '', $document->_script['text/javascript']);
if(str_replace(array("\n","\r","\t", ' '), '', $javaScript) == "jQuery(window).on('load',function(){});")
{
    $javaScript = '';
}

$app = JFactory::getApplication();
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');

$isEditContent = ($option == 'com_content' && $view == 'form' && $layout == 'edit');
$enableLeftCol = (bool)(!$isEditContent && ($this->countModules('search') || $this->countModules('left_col_left') || $this->countModules('left_col_right')));
$isContent = ($option == 'com_content' && $view == 'article');
$isCategory = !$isContent;
if($isEditContent)
{
    $allowedJoomlaScripts[] = '/media/system/js/core.js';
    $allowedJoomlaScripts[] = '/media/system/js/validate.js';
//    $allowedJoomlaScripts[] = '/media/system/js/calendar.js';
//    $allowedJoomlaScripts[] = '/media/jui/js/chosen.jquery.min.js';
//    $allowedJoomlaScripts[] = '/media/jui/js/ajax-chosen.min.js';
//    $allowedJoomlaScripts[] = '/media/system/js/modal.js';
}
?>
<!DOCTYPE HTML>
<html lang="ru">
<head>
	<meta charset="UTF-8">
    <base href="<?php echo $document->getBase(); ?>"/>
    <meta name="keywords" content="<?php echo $document->getMetaData('keywords'); ?>"/>
    <meta name="description" content="<?php echo $document->getDescription(); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title><?php echo $document->getTitle(); ?></title>

	<link rel="icon" type="image/x-icon" href="<?php echo $this->baseurl; ?>templates/jpnew/favicon.ico">
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>templates/jpnew/css/template.css" />

    <?php if(is_array($document->_styleSheets) && count($document->_styleSheets)) : ?>
        <?php foreach($document->_styleSheets as $css => $val) : ?>
            <?php if(in_array($css, $allowedJoomlaStyles) || $isEditContent) : ?>
                <link rel="stylesheet" href="<?php echo $css; ?>"/>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
		
	<script src="<?php echo $this->baseurl; ?>templates/jpnew/js/modernizr.js"></script>

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>templates/jpnew/js/html5shiv.min.js"></script>
	<![endif]-->
</head>
<body>
	
	<!-- topline: start -->
	<section id="topline">
        <jdoc:include type="modules" name="message" style="clear_no_head" />
		<nav class="topmenu">
			<div class="container">
				<div class="grid-wrap">
					<div class="grid-col">
						<a href="/" class="logo">
							<svg viewBox="0 0 100 100" class="svg svg-logo">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-logo"></use>
							</svg>
							<span>Joomlaportal.ru</span>
							<small>Joomla! по-русски</small>
						</a>
                        <jdoc:include type="modules" name="top_menu" style="clear_no_head" />
                        <jdoc:include type="modules" name="top_user_block" style="clear_no_head" />
						<div id="hamburger" class="hamburger-menu">
							<svg viewBox="0 0 100 100" class="svg svg-hamb">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-hamb"></use>
							</svg>
							<span class="user-message">
								<svg viewBox="0 0 100 100" class="svg svg-att">
									<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-att"></use>
								</svg>
							</span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
	</section>
	<!--/ topline: end -->	
	
	
	<!-- mobile-menu: start -->	
	<div id="mobile-menu"><!-- TODO mobile-menu-->
		<ul id="mobile-list">
            <jdoc:include type="modules" name="mobile_menu_user_block" style="clear_no_head" />
            <jdoc:include type="modules" name="mobile_menu" style="mobile_menu" />
		</ul>
	</div>	
	<!--/ mobile-menu: end -->
	
	<!-- header: start -->
    <?php if($isMainPage) : ?>
	<header id="header">
		<div class="container">
			<div class="grid-wrap">
				<div class="grid-col">
					<h1>
                        <jdoc:include type="modules" name="header" style="clear_no_head" />
					</h1>
				</div>				
				<div class="grid-col">
					<div class="header-panel">
						<div class="header-panel-download">
							<svg viewBox="0 0 100 100" class="svg svg-link">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-link"></use>
							</svg>
                            <jdoc:include type="modules" name="download" style="clear_no_head" />
						</div>
						<div class="header-panel-docs">
							<svg viewBox="0 0 100 100" class="svg svg-bookmark">
								<use xlink:href="/templates/jpnew/images/ico/svg-defs.svg#svg-bookmark"></use>
							</svg>
                            <jdoc:include type="modules" name="docs" style="clear_no_head" />
						</div>
						<div class="header-panel-search">
                            <jdoc:include type="modules" name="search" style="clear_no_head" />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>
    <?php endif; ?>
	<!--/ header: end -->
	
	<main id="content">
		<!-- news-line: start -->
        <jdoc:include type="modules" name="top_middle" style="news" />
		<!--/ news-line: end -->
		
		<!-- from-blog: start -->
        <jdoc:include type="modules" name="middle_middle" style="blog" />
		<!--/ from-blog: end -->
        <?php if (!$isMainPage) { ?>
                <div class="container">
                    <div class="grid-wrap <?php echo ($isCategory) ? 'category' : 'item-page'; ?>">
                        <div class="content-col grid-col <?php if($enableLeftCol) { echo ($isCategory) ? 'col-one-half category-left' : 'col-two-thirds item-left'; } ?>">
                            <jdoc:include type="modules" name="breadcrumbs" style="clear_no_head" />
                            <!-- content: start -->
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                            <!--/ content: end -->
                        </div>

                    <?php if($enableLeftCol) : ?>
                        <div class="grid-col <?php echo ($isCategory) ? 'col-one-half category-right' : 'col-one-third item-right'; ?>">
                            <!-- category: start -->
                            <section class="left-col">
                                <?php if ($this->countModules('search')) : ?>
                                    <div class="category-search">
                                        <jdoc:include type="modules" name="search" style="clear_no_head" />
                                    </div>
                                <?php endif; ?>

                                <jdoc:include type="modules" name="left_col" style="clear_no_head" />

                                <div class="left-half-wrapper">
                                    <?php if ($isCategory && $this->countModules('left_col_left')) : ?>
                                        <div class="half-left-col">
                                            <jdoc:include type="modules" name="left_col_left" style="clear_no_head" />
                                        </div>
                                        <div class="half-right-col">
                                            <jdoc:include type="modules" name="left_col_right" style="clear_no_head" />
                                        </div>
                                    <?php else : ?>
                                        <jdoc:include type="modules" name="left_col_left" style="clear_no_head" />
                                    <?php endif; ?>
                                </div>

                            </section>
                            <!--/ category: end -->
                        </div>
                    <?php endif; ?>

                    </div>
                    <div class="clearfix"></div>
                </div>

        <?php } ?>
		<!-- banners-home: start -->
        <?php if ($this->countModules('banners')) : ?>
		<section id="banners-home">
			<div class="container">			
				<div class="grid-wrap banners">
                    <jdoc:include type="modules" name="banners" style="clear_no_head" />
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
        <?php endif; ?>
		<!--/ banners-home: end -->
		
		<!-- bottom: start -->
        <?php if ($this->countModules('bottom_modules')) : ?>
		<section id="bottom">
			<div class="container">			
				<div class="grid-wrap"><!-- TODO forum -->
                    <jdoc:include type="modules" name="bottom_modules" style="clear_no_head" />
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
        <?php endif; ?>
		<!--/ bottom: end -->
	</main>
	
	<!-- footer: start -->
	<footer id="footer">
		<div class="container">
			<div class="grid-wrap">
				<div class="grid-col col-one-third rights-info">
                    <jdoc:include type="modules" name="footer_info" style="clear_no_head" />
				</div>
				<div class="grid-col col-one-third footer-links">
                    <jdoc:include type="modules" name="footer_links" style="clear_no_head" />
				</div>
				<div class="grid-col col-one-third footer-right">
                    <jdoc:include type="modules" name="footer_banners" style="clear_no_head" />
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="grid-wrap">
				<div class="grid-col footer-bottom">
                    <jdoc:include type="modules" name="footer_copyright" style="clear_no_head" />
                </div>
				<div class="clearfix"></div>
			</div>
		</div>

        <?php if ($this->countModules('footer_bottom')) : ?>
		<div class="footer-line">
			<div class="container">
				<div class="grid-wrap">
					<div class="grid-col">
                        <jdoc:include type="modules" name="footer_bottom" style="clear_no_head" />
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
        <?php endif; ?>
	</footer>
	<!--/ footer: end -->
	
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	 <script src="<?php echo $this->baseurl; ?>templates/jpnew/js/grids.min.js"></script>
	 <script src="<?php echo $this->baseurl; ?>templates/jpnew/js/template.js"></script>
    <?php if(is_array($document->_scripts) && count($document->_scripts)) :
        foreach($document->_scripts as $k => $v) :
            if(in_array($k, $allowedJoomlaScripts) || ($isEditContent && $k != '/media/jui/js/jquery.min.js' && $k != '/media/jui/js/jquery-noconflict.js')) : echo "\n";
                $async = $v['async'] ? ' async' : '';
                if($k == '//yandex.st/share/share.js') ?>
    <script type="<?php echo $v["mime"]; ?>" src="<?php echo $k; ?>"<?php echo $async; ?>></script>
            <?php endif;
        endforeach;
    endif;

    if(!empty($javaScript) && !$isEditContent) : ?>
    <script type="text/javascript">
        <?php echo $javaScript; ?>
    </script>
    <?php endif; ?>

    <?php if($isEditContent) :
        if(isset($document->_custom) && is_array($document->_custom) && count($document->_custom))
        {
            foreach($document->_custom as $v)
            {
                echo $v."\n";
            }
        }
        ?>

    <script type="text/javascript">

        Joomla.submitbutton = function(task)
        {
            if (task == 'article.cancel' || document.formvalidator.isValid(document.getElementById('adminForm')))
            {
                Joomla.submitform(task);
            }
        }
        <?php echo $document->_script['text/javascript']; ?>
    </script>
    <?php endif; ?>


</body>
</html>